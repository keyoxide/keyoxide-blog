+++
title = "Now on Android!"
date = "2021-11-11 17:00:01"
+++

## The Keyoxide Android client app

I'm very excited to announce there's now an Android client app for Keyoxide! 

Just like the website, it can view Keyoxide profiles and display the verification of identity claims! But now in a more mobile-friendly way :)

It's still a work-in-progress and doesn't do everything yet (see **Roadmap**) but it's a good start to another way of interacting with decentralized identities.

A huge huge thanks to [@berker](https://fosstodon.org/@berker) without whom none of this would exist, he wrote the entire project in four weeks!

## A Flutter project

The [source code](https://codeberg.org/keyoxide/keyoxide-flutter) for this project was written using the [Flutter](https://flutter.dev/) toolkit which means that if there's demand for it, the app could also be generated for iOS devices. Do let us know if you'd like to see an iOS app happen:

- on the fediverse: [@keyoxide.org@fosstodon.org](https://fosstodon.org/@keyoxide)  
- on the matrix network: [#keyoxide:matrix.org](https://matrix.to/#/#keyoxide:matrix.org)  
- on the issue tracker: [codeberg.org/keyoxide/keyoxide-flutter](https://codeberg.org/keyoxide/keyoxide-flutter/issues)  
- via email: [yarmo@keyoxide.org](mailto:yarmo@keyoxide.org)  

## Roadmap

There are a few things we are considering or working on that would be useful additions to the app:

- Open app when 3rd party QR reader app scans an OpenPGP QR code
- Integrate QR reader in app
- Add message encryption and signature verification
- Support fully local claim verification

## Antiroadmap

What this app is not intended to be: an OpenPGP client or key management app. We know that editing keys and adding notations is still a complex procedure and would love nothing more than to have a simple app that handles all this for you in an easy-to-use package.

This app will do what the website can and so in a mobile-friendly way, but nothing more. For the safety of all our secret keys.

That doesn't mean we aren't having discussions around ideas on how to design and implement a secure key/identity management app. If you're interested and perhaps willing to help in this domain, be sure to join the matrix channel or start/join a discussion on the [keyoxide-devel mailing list](https://lists.sr.ht/~yarmo/keyoxide-devel).

## Download

Here's the [APK link](https://codeberg.org/attachments/8714ab36-118b-48e3-97a6-fbc0e8ba7fc8) ([release 1.0.1](https://codeberg.org/keyoxide/keyoxide-flutter/releases/tag/1.0.1)).

The app is currently going through the [F-Droid](https://f-droid.org/) inclusion process and will hopefully be available on there soon! We're still on the fence about the benefits of putting the app on the Play Store. Again, feel free join the discussion and voice your opinion :)

## Signing off

Another big thank you to [@berker](https://fosstodon.org/@berker) for the amount of work and dedication you put into this project in such short time. Cheers mate!

Hope you all enjoy the app and be sure to catch some sun if you get the chance to!

Until later,  
Yarmo