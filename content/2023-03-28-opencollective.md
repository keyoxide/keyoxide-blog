+++
title = "Live on OpenCollective!"
date = "2023-03-28 17:17:22"
+++

## Working as a Collective

The Keyoxide project is now live on [OpenCollective](https://opencollective.com/keyoxide)! This will make it easier to receive donations, pay the monthly costs of running the servers and even opens the possibility to reward important code contributions!

As per the model of OpenCollective, all transactions are public and auditable. Many thanks to [Open Collective Europe](https://opencollective.com/europe) for hosting the project!

To celebrate this moment, Keyoxide has added the ability to verify OpenCollective accounts! See it in action here on the [project's Keyoxide page](https://keyoxide.org/44a875dcbef777e79d7cca3ecc5764283d461ad1).

If you like Keyoxide and want to see it go further, please consider making a donation on our [OpenCollective](https://opencollective.com/keyoxide), thank you.