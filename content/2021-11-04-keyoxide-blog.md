+++
title = "Keyoxide blog is now live"
date = "2021-11-04 14:52:10"
+++

Hey there, welcome on the new dedicated Keyoxide blog!

It's a bit barebones—one might say *minimalist*—but it's a blog.

And statically generated.

Yes, I thought it was time to move Keyoxide announcements away from my [personal blog](https://yarmo.eu) and give them their own space on the web.

## A small update

The Keyoxide project has been silent lately, very silent in fact. But rest assured, the time was well spent, several subprojects were simultaneously demanding my attention and this led to all of them being somewhat delayed.

Upside of this: there will be multiple announcements in the coming days as all these separate efforts seem to near completion soon :)

Yarmo