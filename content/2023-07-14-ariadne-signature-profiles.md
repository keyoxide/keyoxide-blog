+++
title = "In testing: Ariadne Signature Profiles"
date = "2023-07-14 16:58:29"
+++

I am delighted to tell you about the new Ariadne Signature Profile (ASP), the solution to creating a profile on Keyoxide without needing to use OpenPGP or the computer's command line interface.

## TL;DR

ASPs are a new type of Keyoxide profile that are easier to create.

ASPs are still "in testing", things may change and profiles created during the "testing phase" may not work after the official launch — though I will do everything possible to prevent that from happening!

Go to [asp.keyoxide.org](https://asp.keyoxide.org) and follow the instructions there to create your own profile with just a few clicks.

To verify ASP profiles, use [dev.keyoxide.org](https://dev.keyoxide.org). Here's an [example ASP profile](https://dev.keyoxide.org/aspe%3Akeyoxide.org%3A6WJK26YKF6WUVPIZTS2I2BIT64).

If you have feedback or noticed a bug, please do [reach out](https://docs.keyoxide.org/getting-started/community/)!

## The non-technical introduction to ASPs

Currently, to create an identity profile people can verify with Keyoxide tools, one must create a so-called "OpenPGP cryptographic key", store their identity claims (like "I claim to be the user @yarmo on https://fosstodon.org") inside this key and share that public key using keyservers.

Even the non-technical introduction gets technical quickly because it fundamentally is. There are tools to help with this process but they are not easy to use at all. Even after years of using them, they still confuse me from time to time. I wanted to create my own tools to make the process easier, but not a lot of developer libraries support the functionality that Keyoxide needs.

This situation has led many people to ask for easier tools and discard the Keyoxide project in the meantime.

For the past year, I have been working on my solution to this problem: a type of profile that does not rely on OpenPGP keys and thus allows us to create nice tools and apps to let people create their own profile.

These are the Ariadne Signature Profiles, or ASPs. Creating one can now be as simple as clicking a few buttons.

## Easier to use

The simplest way right now to create an Ariadne Signature Profile is to go to [asp.keyoxide.org](https://asp.keyoxide.org) and follow the instructions there!

In short, click the "Create a new profile" button and the tool will ask you for a password, that is all you need to create a profile!

Important note! After creating a profile, the tool will give you your "encrypted secret key". You need to store this somewhere on your computer or password manager as you will need this — together with your password — to update your profile. If you lose this "encrypted secret key", you will not be able to update your profile in the future.

I know it sounds inconvenient but it's an extra safety measure as it is bad practice to let servers touch secret keys! They are yours and yours alone and should not be shared or uploaded.

The ASP web tool is a work-in-progress and will get better with time, especially the translations so that more people may be able to use it in other languages.

## Local tools

If you prefer local tools, I hear you! There is also the [kx-aspe-cli](https://codeberg.org/keyoxide/kx-aspe-cli) command line interface, which also encrypts secret keys with passwords for safety. The CLI tool is still a work-in-progress and may be a bit rough around the edges, but it works!

There are no desktop apps yet as this is simply not my area of expertise. But if it is yours, feel free to experiment with ASPs, they're quite simple to work with!

Now that the web tool and the CLI are in public testing, we are going to work on making it possible to create profiles with the Keyoxide mobile app, which will make the process even easier! News on this will be posted on the blog as soon as possible.

## What about OpenPGP profiles?

They are not going anywhere. Keyoxide was always built with the idea of giving people the choice on how they wish to create their profile. And under certain circumstances, an OpenPGP profile may be more desirable than an ASP. So it's up to you. Keyoxide will always support both types of profiles. And whatever type of profile comes next ;)

## Verifying ASP profiles

Right now, ASP profiles can only be verified using [dev.keyoxide.org](https://dev.keyoxide.org/). Soon, I'll enable it on the main [keyoxide.org](https://keyoxide.org/) instance.

Here's an [example ASP profile](https://dev.keyoxide.org/aspe%3Akeyoxide.org%3A6WJK26YKF6WUVPIZTS2I2BIT64).

## Documentation

There is no user documentation yet on regarding ASPs, I am working on this and will publish it for the official ASP launch.

For developers, there is the [ASP spec](https://ariadne.id/related/ariadne-signature-profile-0/).

Everyone is of course welcome to join the [community](https://docs.keyoxide.org/getting-started/community/) for any questions or feedback they might have! 

## Conclusion

Once we officially make ASPs stable, we'll have truly lowered the barrier of entry to creating Keyoxide profiles and made decentralized online identity verification accessible to a broader audience.

If you like Keyoxide and want to see it go further, please consider making a donation on our [OpenCollective](https://opencollective.com/keyoxide), thank you.
