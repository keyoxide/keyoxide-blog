+++
title = "XMPP Proofs: now better than ever!"
date = "2022-12-12 16:09:19"
+++

## TLDR

XMPP identity verification through Keyoxide is fixed and now works better than before!

Use the **XMPP Ariadne Proof Utility** ([xmpp-util.keyoxide.org](https://xmpp-util.keyoxide.org/)) to upload and manage your identity proofs in your XMPP account.

## Introduction

Soon after writing my [Keyoxide, XMPP claims and vCard](/xmpp-claims-vcard/) blog post, I was contacted by Matthew Wild, Prosody developer and maintainer of [Snikket](https://snikket.org/) and he told me (paraphrasing for brevity but he was a lot more cordial!):

> You've got the facts right but the whole system wrong.

What did I get wrong exactly?

## Misusing the vCard system

Since Keyoxide needs to fetch some public data location of your XMPP account to find your identity proof, I chose to store identity proofs in the vCard data. At the time, this was publicly available through [XEP-0054: vcard-temp](https://xmpp.org/extensions/xep-0054.html) and thus a good fit for Keyoxide.

But it is important to remember that hosting publicly available data is NOT the purpose of the vCard system: it is made for hosting personal data and sharing it with your contacts.

[XEP-0292: vCard4 over XMPP](https://xmpp.org/extensions/xep-0292.html), to which the XMPP ecosystem is transitioning to, fixes this with a configurable access model that defaults to contacts-only access. A great development for the XMPP users! But not so great for Keyoxide.

In my [Keyoxide, XMPP claims and vCard](/xmpp-claims-vcard/) blog post, I explained how I was trying to fight the vCard system and how I figured I lost.

Then came the incredibly helpful Matthew Wild who taught me how to use the system to my advantage.

## Seeing PubSub in a different light

Talking to Matthew, I learned among many other things that [XEP-0163: Personal Eventing Protocol](https://xmpp.org/extensions/xep-0163.html) is an extremely versatile beast, is used by XMPP clients to store preferences and can even be used to store public data as described in [XEP-0222](https://xmpp.org/extensions/xep-0222.html).

In other words, why try and misuse the vCard data if we can just store the identity proof data in a different space where it can be made public?

It all seems so obvious now.

## The new XMPP identity Proof

So, from now on, that is exactly how XMPP identity verification is going to work: identity proofs are stored directly using the PubSub protocol in the XMPP account, in a dedicated public PubSub node that anyone can fetch to verify your identity.

To upload and manage identity proofs on your XMPP account, I am releasing the **XMPP Ariadne Proof Utility**, hosted at [xmpp-util.keyoxide.org](https://xmpp-util.keyoxide.org/). It is of course [open source](https://codeberg.org/keyoxide/xapu), and built on top of the [XMPP account exporter](https://github.com/snikket-im/xmpp-account-exporter) utility built by Matthew Wild.

The utility runs entirely in the browser and no personal data or credentials are stored or shared. It's a static HTML page so feel free to host it yourself or simply download it and open it locally.

Of course, Keyoxide will still check vCard data for backward compatibility, but I strongly recommend switching over to the new vCard-less identity proofs.

## A lesson for the future

There is something much bigger to be learned here: talk more and earlier with knowledgeable people and experts, and don't hesitate to do so.

Sometimes, Keyoxide is hampered because services don't have public APIs or lack the necessary infrastructure to support hosting proofs. For example, it is often needed to publish identity proofs inside an account's Biography section to make it publicly available, something for which the Biography section was not designed.

And indeed, up until now, XMPP didn't have dedicated infrastructure for hosting identity proofs.

But instead of going ahead and try misusing existing infrastructure, knowledgeable people and experts can help better understand the existing infrastructure and use it to one's advantage. And this was without a doubt the case here.

## Signing off

A huge thanks to Matthew Wild for helping me understand XMPP and PubSub to a much higher degree and assisting me in the creation of this utility.

It fills me with joy to know that Keyoxide can now verify XMPP accounts and that we won't need to worry about anything anymore: no vCard transitioning, no server-specific configurations. [Nearly all XMPP servers support XEP-0163](https://compliance.conversations.im/test/xep0163/) and thus support proving accounts with identity proofs uploaded and managed with tools such as [xmpp-util.keyoxide.org](https://xmpp-util.keyoxide.org/).

Happy XMPP account proving to all!

Yarmo