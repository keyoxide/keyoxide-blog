+++
title = "Keyoxide Project Update #3"
date = "2021-03-09 16:00:00"
+++

Two months since the last update. Two great new additions for this one.

## Keyoxide 2.5.0

The latest version of Keyoxide's web client has two very neat additions, all
thanks to the latest [0.11.*](https://codeberg.org/keyoxide/doipjs) release of
[doip.js](js.doip.rocks): the verification of accounts on the IRC and Matrix
platforms.

This really is what Keyoxide was designed to do: making sure your online
correspondances are exchanged with the intended person or entity, even as both
parties use anonymous accounts with varying usernames on different platforms.

To this end, it was important to integrate additional communication platforms,
to join the already supported [XMPP](https://keyoxide.org/guides/xmpp) protocol.

Given the popularity and decentralized nature of either platform, IRC and Matrix
were both prime candidates. So, let's see what goes into proving identities on
IRC and Matrix.

(All examples below use a certain OpenPGP fingerprint and a fictional user, make
sure to use your own data when replicating the steps!)

## Proving IRC identity with taxonomy

What is taxonomy within the context of IRC?

```
>> /msg NickServ help TAXONOMY

***** NickServ Help *****  
Help for TAXONOMY:

The taxonomy command lists metadata information associated
with registered users.
 
Examples:  
    /msg NickServ TAXONOMY foo  
***** End of Help *****
```

Taxonomy is metadata information, much like the vCard data for XMPP. Let us use
this to our advantage:

```
>> /msg NickServ SET PROPERTY KEY openpgp4fpr:3637202523e7c1309ab79e99ef2dc5827b445f4b

Metadata entry KEY added.

>> /msg NickServ TAXONOMY foo

Taxonomy for foo:
KEY              : openpgp4fpr:3637202523e7c1309ab79e99ef2dc5827b445f4b
End of foo taxonomy.
```

And there you have it: one-directional linking from IRC to an OpenPGP key. Now,
to make that bidirectional, add the following notation to your key:

```
proof@metacode.biz=irc://chat.freenode.net/foo
```

Et voilà, foo has now cryptographically proven that that IRC nickname is theirs.

All steps above are explained with more detail in the
[IRC guide](https://keyoxide.org/guides/irc).

It is important to note that IRC is the slowest identity to verify to date. As
IRC servers lack API endpoints to query the taxonomy metadata (maybe one
day? ^_^), Keyoxide has to log in into the IRC server like any other client,
send a message to NickServ, parse the response and log out again.

## Proving Matrix identity with...

[Matrix](https://matrix.org/) is an excellent decentralized communication
platform, but sadly, for identity verification purposes, it completely lacks
any form of customisable metadata. A shortcoming we can work with, but one which
might also understandibly deter some from proving Matrix identities.

One simply sends a message to a public room with the following content:

```
[Verifying my OpenPGP key: openpgp4fpr:3637202523e7c1309ab79e99ef2dc5827b445f4b]
```

Easy enough. The issue is: Keyoxide can only read messages in a public room via
a Matrix account that already has access to said public room.

Ergo, we can't just use any room, we'll all have to use the same room.

A dedicated room named
[#doipver:matrix.org](https://matrix.to/#/#doipver:matrix.org) has been created
for the very purpose of receiving Matrix identity proofs. Simply join the room
and send the message with your own OpenPGP fingerprint.

By viewing the source of message, you get the data needed to generate the
identity claim to be stored inside your OpenPGP key: the `room_id` (shared by
everyone) and the `event_id` (unique to your proof). The notation will look
like this:

```
proof@metacode.biz=matrix:u/@foo:matrix.org?org.keyoxide.r=!dBfQZxCoGVmSTujfiv:matrix.org&org.keyoxide.e=$3dVX1nv3lmwnKxc0mgto_Sf-REVr45Z6G7LWLWal10w
```

That is quite an unwieldy notation, but one designed to follow the
[MSC2312 Matrix URI scheme proposal](https://github.com/matrix-org/matrix-doc/pull/2312)
.

Please refer to the [Matrix guide](https://keyoxide.org/guides/matrix) for
detailed instructions on how to verify your own Matrix identity.

## Signing off

As the project now supports multiple communications platforms and its
versatility increases with each update, I am confident that Keyoxide is now
ready for the next phase: focus on the user experience. Nothing to show just
yet, but I am sure the next project update will have interesting announcements
related to this.

For all your questions and suggestions, be sure to join the conversation in the
[Keyoxide matrix room](https://matrix.to/#/#keyoxide:matrix.org).

Until next time.
