+++
title = "Launching the Ariadne Spec"
date = "2021-11-07 19:54:45"
+++

TLDR: the Ariadne Spec is an experimental community-driven living document on cryptographic decentralized identity verification.

## The abstraction of an abstraction

From the get-go, Keyoxide was designed to be about the process of identity verification, not about itself. And if you want to create an open ecosystem, you've got to make it welcoming.

So, not soon after launching Keyoxide, we extracted the identity verification code and released it as a separate library named [doip.js](https://codeberg.org/keyoxide/doipjs) to make it easier for other websites and apps to start using OpenPGP identity proofs too—without having to rely on Keyoxide.

But even with a website-agnostic library, it didn't feel abstract enough: projects still would need to use our library, with our code design choices, written in a language of our choice.

Why not publish the essence of the decentralized identity verification process? Not the code, but the underlying knowledge.

## The Ariadne Spec

That is the aim of the experimental Ariadne Spec, available at [https://ariadne.id](https://ariadne.id).

From the [Keyoxide documentation](https://docs.keyoxide.org/getting-started/what-is-ariadne-id/):

> The Ariadne Spec is our attempt to:
> 
> - create a community-driven living document that explains how decentralized online identities should work, and
> - create an ecosystem of code and apps that no single entity rules but everyone can benefit from.
> 
> In short, it's a collection of text documents called ARCs that can be written by anyone willing to participate and are then democratically voted upon before being integrated into the Ariadne Spec. The ensemble of these ARCs is the Ariadne Spec(ification).
> 
> Since the Ariadne Spec's content is continuously subject to change, it's called a "living" document.
> 
> The Ariadne Spec is also "community-driven" because anyone can contribute, not just the people involved in the Keyoxide project.
> 
> This means that Keyoxide is really just an implementation of the Ariadne Spec: an actor in an ecosystem that could never be dominated by a single entity since the knowledge and its governance are open and accessible by all.
> 
> And that is just how we like our digital ecosystems.

The entire ARC process is described in detail in [ARC-0001](https://ariadne.id/arc-0001.xml).

## proof@ariadne.id=

Given the fact that the knowledge on decentralized identity verification is now stored on its own domain, we can now use that domain as the new namespace for claim notations.

Or, in normal language, all claims will now look like this:

```
proof@ariadne.id=https://someservice.tld/user
```

A much requested feature since the beginning one year ago 🥳 but one which needed a lot of effort and thought to get right.

Oh, and all your "old" `proof@metacode.biz=` notations? No worries, they all still work and will always continue working!

## So it's all working, then?

Euhm, mostly.

We've decided to somewhat embrace the "release early, release often" paradigm on this one. The entire ARC process is still a little experimental and in progress. We've tried to adopt the best lessons and practices from processes like RFC and XEP and stick them in a low-barrier-to-entry implementation.

Mistakes will be made, but I believe in the collective know-how and determination of the people. Together, we are able to shape this process and democratically gather the knowledge needed to verify our online identities.

Even more important, this process must be fun. Plain, simple fun. Not enough of that going around in software development these days.

## Documentation-as-a-project

So, there it is. The [Ariadne Spec](https://ariadne.id). Our guide through the maze of online service providers and the diverse ways of proving our hold over our accounts on them. A path to claiming back sovereignty over our online identity.

A project that delivers documentation, not code.

Hope you like it. Hope it inspires some of you to go out there, log into your favorite online service, find a way to externally verify your hold over your account and share this knowledge through the Ariadne Spec.

Also hope it inspires some of you to start building additional librairies and projects based on this knowledge.

Yarmo