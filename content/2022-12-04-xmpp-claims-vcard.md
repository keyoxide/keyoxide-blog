+++
title = "Keyoxide, XMPP claims and vCard"
date = "2022-12-04 11:31:27"
updated = "2022-12-12 16:09:19"
+++

## TLDR

XMPP identity verification through Keyoxide is no longer possible for the foreseeable future due to a period of transitionining between vCard standards in the XMPP ecosystem.

**UPDATE ON 2022-12-12**: XMPP account verification has been fixed and improved, here's the [blog post](/xmpp-proofs/).

## Introduction

Back when Keyoxide started in 2020 and I added verification of XMPP accounts, it didn't work too well. It turned I didn't fully understand XMPP and vCard. A few fixes later, it worked much better.

For about a month now, I have been receiving more and more complaints about XMPP identity verification no longer working for many. I couldn't look into it at first due to a busy schedule, but I have been doing some digging the last few days and I think I understand what is going wrong. Sadly, due to the nature of the issue, it won't be easy to fix and this will probably mean for the foreseeable future, people won't be able to add XMPP claims to their Keyoxide profiles.

I have been talking to XMPP developers and their words and advice have led me to the conclusions in this post. If I have misunderstood anything or failed to see the entire picture, do let me know and I will correct the post.

## How Keyoxide and XMPP work together

As with all Keyoxide cl2022-12-03-xmpp-claims-vcardims and proofs, identity is established through bidirectional linking: your cryptographic key points to your XMPP account, your XMPP account points back to your key.
2022-12-03-xmpp-claims-vcard
I am a latecomer to the XMPP party so I do not pretend to know the entire lore, but from what I can gather, XMPP *official* support for vCard started in 2002 with [XEP-0054 vcard-temp](https://xmpp.org/extensions/xep-0054.html). It states:

> This specification documents the vCard-XML format currently in use within the Jabber community. A future specification will recommend a standards-track protocol to supersede this informational document.

It appears XMPP servers were already implementing vCard in a non-standardized way, this XEP was meant to standardize the practice temporarely until a new more intentional XEP was published and solved all vCard issues. XEP-0054 was last updated in 2008.

This brings us to [XEP-0292 vCard4 over XMPP](https://xmpp.org/extensions/xep-0292.html), first published in 2010. It states:

> This document specifies an XMPP extension for use of the vCard4 XML format in XMPP systems, with the intent of obsoleting the vcard-temp format.

The XEP has been worked for a couple of years but for reasons that I do not know, it has not been adopted twelve years later and in fact is now in a "deferred" state — I have been assured by a XMPP developer this doesn't actually mean anything for the future of the XEP. XEP-0292 is likely the candidate to supersede vcard-temp, the timeline for this is simply unclear.

So, what should XMPP servers and clients support? Again, I do not have a definitive answer but it appears it is up to the individual projects to decide which "standard" to support.

## The old complaints

Receiving complaints from people not getting their XMPP claims and proofs to work is nothing new to the project. As I have mentioned before, especially in Keyoxide's first year, XMPP support was pretty rough.

For example, it was only after a while that I found out some clients/servers store vCard notes as a "note" node, and others as a "desc" node. With Keyoxide now configured to look into both nodes, this solved the issue for many. But still, some were experiencing identity verification issues.

Later still, I found out about Prosody's [restrict vCard access](https://prosody.im/doc/modules/mod_vcard_legacy) setting. If the server2022-12-03-xmpp-claims-vcardl was fine again.

## The new complaints

Since about a month ago, I have been receiving more XMPP-related complaints. The people followed the guides and Keyoxide would still refuse to verify their accounts.

Once I looked into the issue, I found something surprising: their vCard data that I obtained through vcard-temp was not the data they claimed to have put there. vcard-temp would give me their old vCard data. I thought this could be a rogue server issue, but then found out it happened to people on different servers. And it happenend to people on the same server that I use. And yet my identity verification was working fine!

A few days ago, I finally sat down and started debugging. My XMPP identity proof was still working!

I use [Dino](https://dino.im) as my daily client but it doesn't allow one to change their vCard data, so I installed [Gajim](https://gajim.org). Can I just say, what a wonderful client that has become!

Anyway, oddity #1: I clicked on the **Profile** button where one goes to update vCard data and found it… Empty. Strange. I *did* have vCard data. I checked my public page on [mov.im](https://mov.im), yup, I really did still have vCard data. Why isn't Gajim showing that data?

I didn't think much of it and entered some of my personal details again. Including a new note that would act as identity proof.

It updated successfully and the new note was also displayed on mov.im.

Oddity #2: I asked Keyoxide to fetch my vCard data through vcard-temp and it received… The OLD VCARD DATA!

Oh, so the server must have switched to vCard4 then?

Oddity #3: I asked Keyoxide to fetch my vCard data through vcard4-over-xmpp and I received… An error message that vCard4 was not supported on this server.

I was baffled. Where did my vCard data go to? And how come mov.im and Gajim know the new data, and I still get old data?

I sent a desperate message to the fediverse, was recommended to talk to a XMPP developer room and that's when everything became clear.

## The lengthy vCard transition

As I mentioned earlier, it appears XMPP projects decide for themselves whether to continue supporting vcard-temp or switch to vCard4. And the tools I recommend for Keyoxide appear to have made the switch.

*But wait, I thought the server said vCard4 was not supported?*

Apparently, I have been asking the server for the wrong thing.

Allow me to introduce you to [XEP-0060 Publish-Subscribe](https://xmpp.org/extensions/xep-0060.html). As I have had little time to mentally wrestle with this fella, I'm not 100% certain I see the entire picture here, but it appears that's how XMPP projects expose their vCard data nowadays.

The way I read *XEP-0292 vCard4 over XMPP*, it proposes two methods of obtaining vCard4 data:

- [IQ calls](https://xmpp.org/extensions/xep-0292.html#self-iq)
- [event notifications](https://xmpp.org/extensions/xep-0292.html#self-pubsub) through the aforementioned Publish-Subscribe

I tried the IQ calls for vCard requests on several servers but none of them supported it. I tried the event notifications and lo and behold, the missing updated vCard data surfaced!

Conclusion? Two years ago, Gajim and mov.im must have supported vcard-temp and that's how I entered my vCard data back then. This is also how Keyoxide retrieved the vCard data used to verify accounts.

Somewhere between two years ago and today, both Gajim and mov.im must have transitioned to support vCard4 — through PubSub, not IQ calls! Indeed, this was the case for [Gajim 1.3.0](https://gajim.org/post/2021-02-08-gajim-1.3.0-released/), released early 2021.

So now, Keyoxide should simply switch to vCard4 with PubSub and all is well. Right?

## A happy ending?

Well, this is where things get awkward.

See, the old vcard-temp system had a bit of a flaw: data was always public — unless you used Prosody's *restrict vCard access*. Granted, this flaw is what allowed Keyoxide's identity verification to work in the first place, but still, publicly exposing your vCard data should at the least be opt-in.

The new vCard4 system fixes that — or so I have been told that one day it will. Remember that we can only access the vCard4 data through the Publish-Subscribe interface? Well, acquiring data through Publish-Subscribe requires a "Subscription". A Subscription is easily requested but needs to be approved by the account we subscribe to.

This means that in order to verify a XMPP account, I need my Keyoxide XMPP verifier account to subscribe to that XMPP account, wait for them to accept it and only then it will work. And that is not acceptable behavior.

First of all, Keyoxide does not know beforehand which XMPP account holds an identity proof and which don't, meaning a bad actor could simply make my Keyoxide XMPP verifier account spam subscriptions to many XMPP accounts that want nothing to do with account verification.

And second, Keyoxide is decentralized. Each Keyoxide instance needs a separate XMPP verifier account. And each XMPP verifier account will also need to subscribe to each and every XMPP account.

This is obviously a lose-lose situation. Keyoxide can only use publicly available vCard data lest it becomes a spam account.

And currently, there is no other way to obtain publicly available vCard data. The XMPP ecosystem has entered the awkward phase of the vCard transition where the major clients no longer support vcard-temp but servers only partially support vCard4 over XMPP. This is not enviable position for the ecosystem to be in and I wish the XMPP developers all the strength to push through this phase and come out with a solid vCard system.

As for Keyoxide, verification of XMPP accounts used to work, and will again at some time in the future, but in the current state of affairs, it doesn't. Or at least not easily.

## What now?

For those with already working XMPP account verification, great! Don't change your key and your proof will continue to work.

For those who wish to verify a new XMPP account, I currently cannot help you, sorry. The tools no longer exist.

While I do not envy the XMPP developers for this unavoidable awkward phase, I will provide one piece of constructive criticism. By transitioning the major XMPP clients from vcard-temp to vCard4 from one version to the next without a period of supporting both at the same time, it has been made impossible for people to actually migrate their vCard data. Yes, I can just enter my new data as vCard4 but I can no longer delete my vcard-temp data! As I have been led to believe that privacy concerns play a role in this transition, I find it odd that it is deemed acceptable to let my old vcard-temp exist in a read-only state for all to see and not give me the tools to delete it.

How is that *constructive* criticism? Well, I believe there's an opportunity here: I think the XMPP community should build a dedicated tool to delete vcard-temp data. I for one would use such a tool once the vCard transition is considered complete.

And I will not lie: if such a tool could also edit vcard-temp data, that would of great benefit to the Keyoxide project and the people wishing to verify their accounts.

Note: there already could be such a tool out there that I simply haven't found yet. If so, please do let me know and I will update this post accordingly.

In my imagination, such a tool could take the form of a Gajim plugin or at the very least, export the XML commands to be entered in the Gajim XML Console.

## Signing off

So there you have it. To the best of my knowledge, people can no longer verify XMPP accounts until there's either a vcard-temp editor tool, or vCard4 data can be accessed publicly without the subscription requirement.

Again, I wish the XMPP community nothing but the best in this transition period.

When the situation improves, I'll be sure to post an update!

**UPDATE ON 2022-12-12**: XMPP account verification has been fixed and improved, here's the [blog post](/xmpp-proofs/).

Yarmo