+++
title = "A nontechnical introduction to Keyoxide and online identity verification"
date = "2023-03-07 13:05:22"
+++

## Introduction

This nontechnical introduction was originally given as a lightning talk at FOSDEM 2023 on February 5th, the recording of which can be viewed below. This post is the written counterpart of that talk.

<video controls>
    <source src="https://video.fosdem.org/2023/H.2215%20(Ferrer)/keyoxide.webm">
</video>

## The IRL passport

Before we tackle online identity verification, let's have a look at in-real-life identity verification.

<img src="/images/cartoon-irl-passport.png" alt="Cartoon about in-real-life passports" style="max-width: 360px">

A passport is used as an identifying document but is a nonfunctional document in and of itself. Granted, it contains a picture of the holder and the paper itself usually has holographic protection, but assuming an actor with sufficient resources and motivation, these can be convicingly imitated. 

A passport gains function when it is verified and checked with the government that issued it to ensure the document is authentic. But even then, this guarantees the document is authentic and still valid, but does not ensure the person presenting it is the same as whom it was issued to.

Passports are difficult-but-not-impossible to counterfeit and aren't actually tied the person to whom it was issued. Could we do better if we were trying to design an identity system for our digital selves?

## Do we need online identity?

Whether we like it or not, the digital realm is slowly but surely invading our lives in the physical world. And more and more crucial human interactions are happening through digital pathways. While some of those might arguably have been better left untouched by the internet, others could have never existed without it: think of remote working and online conferences attended by people worldwide.

Not all online interactions need verifiability, but it is hard to imagine some without: think of organizers of dissent. They may need to use different platforms of communication and thus it is imperative to know who you are talking with on each of them, to avoid communicating the right information to the wrong people. And in the unfortunate case of compromised communication, they wouldn't want their real life identity associated with their online identity.

Maintaining verifiable online identities present unique challenges but by combining the appropriate tools and techniques, it is possible to offer tailored solutions.

## A simple example scenario

<img src="/images/cartoon-kx-rationale.png" alt="Cartoon about why we need online identity" style="max-width: 360px">

Let's imagine two characters, Alice and Bob. Note that these may or may not be their real names. They have been speaking to each other for years using Matrix.

On a day, Alice gets a message from a certain "Bob91" on a fictional network named XYZ. Alice wonders if this is an account made by her friend Bob. But how could she confirm this?

Assuming she currently is unable to directly ask Bob on Matrix, she needs a way to verify Bob's different online identities. She needs a tool that isn't necessarily tied to a real-life identity, but which proves beyond doubt that the same person who created the "Bob" account also created the "Bob91" account on XYZ.

Of course, the opposite must also be true: the tool must be resistant to impersonation attacks, making sure that someone isn't able to successfully verify accounts they did not create or do not manage.

## Requirements and constraints

So, assuming we were to create the digital equivalent of the passport. What would it look like? What would it do? How would different parties interact with it?

The most basic requirement - and also conceptually the most challenging - is **uniqueness**. Something about our hypothetical digital passport should be unique to prevent impersonation. If a passport is not unique, anyone could generate that passport and assume anyone else's identity.

This is challenging since a passport also needs to be **verifiable** and thus **shareable**. In the digital world, there is no sparsity. Any sequence of zeros and ones can be copied and replicated an infinite amount of times. Share a photo online with 10 friends. They could all right-click on that photo and store a copy it on their devices. What started as one picture is now eleven pictures, perfectly identical and indistinguishable. Nothing about the original made it unique, and thus all are now the original.

What we need is some secret information that we could keep to ourselves, and a way to generate some derivative information that is somehow linked to our secret information but does not reveal it. What we need is a one-way function. Luckily, there are solutions for this thanks to cryptography, more on this in a later blog post.

If implemented correctly, this uniqueness will make it **secure**, ensuring that only one person can write the identity information and yet everyone can read it. And no one else can modify that identity data.

Another important requirement is **self-sovereignty**. If we can trust the uniqueness of the passport, we don't need to have to trust or involve an external authority. Every digital citizen should be able to issue their own piece of identification.

An interesting requirement flows directly from self-sovereignty: **anonymity**. If one is able to issue their own passport, what prevents them from issuing multiple passports?

The answer is *nothing*. And that is not a flaw. We can be and indeed are different beings in the digital realm and this should be celebrated and protected.

Now, we know what requirements our identity solution has to meet. Let's have a bird's eye view of how all this can be accomplished with Keyoxide.

## Introducing Keyoxide

Let's go back to Alice and Bob.

Bob needs a way to let others verify his online identities. Sure, he could take the easy way and just create a single google account and rely entirely on that. After all, through that account, he has access to a lot of google services, and plenty more where he can just log into with his google account.

Fortunately for him, he is aware people lose their google accounts daily due to misfiring automated systems and knows that once this happens, there is usually no way to get it back. All your logins, all your mails, all your contacts, all your files and photos, gone because of bad AI.

Bob wants to be in charge of his own online identity and not rely on a big internet corporation.

Using so-called cryptographic tools, Bob creates a cryptographic keypair. For the purpose of this nontechnical introduction, let us visualize the cryptographic keypair as a glass vault.

As its name suggests, a keypair consists of two files. A secret key and a public key. Only Bob has access to the secret key, and thus can store things inside the glass vault. Bob will give the public key to anyone who wants it, allowing them to see inside the glass vault, but no one other than Bob can change what is stored inside the vault.

So, what does Bob choose to put inside the vault?

He writes a document that contains his name (real or not!) and a list of all his accounts: his Matrix account, his Fediverse accounts, his Twitter account if he still has it, etc. And this document, he stores in his glass vault. He then uploads the public key to a keyserver so that is available for everyone to see.

Is Bob done? Not really. Of course, everyone now knows which accounts Bob claims to use. But that is all they are: claims.

Let's meet Mallory. Mallory is trying to scam Alice by pretending to be Bob. So she does the same thing Bob just did: generate a cryptographic keypair, write a document with a list of all of Bob's accounts (and one of his own!) and upload it to a keyserver.

How could Alice ever know which keypair is the right one? She can't unless the claims are somehow verified.

It is important to know that each keypair (glass vault) has a unique identifier, known as a fingerprint. Without going into details, this system is practically safe and ensures every key really has a unique identifier: for SHA2 with 256 bits, there are 115792089237316195423570985008687907853269984665640564039457584007913129639936 (around 116 quattuorvigintillion, give or take) possible fingerprints, and it can go higher than that!

So yeah, the chance of two people accidentally getting the same fingerprint is fairly small.

Bob takes his keypair's fingerprint and puts in his accounts: he adds it to his Fediverse account's Biography, he sends it as a tweet.

Now, Alice has a way to distinguish Bob's real key from Mallory's fake key. Both keys contain a reference to the Fediverse account, but the Fediverse account only has a reference back to a single key. So that has to be Bob's key.

Unless of course Mallory has broken into Bob's Fediverse account, but then Bob has bigger issues.

So that is how Bob creates his online identity and allows others to verify it, while at the same time warding off imposters.

Notice how this post hasn't mentioned Keyoxide yet. That's because Keyoxide is not strictly necessary for this whole process. This method of online identity verification relies entirely on existing tools, infrastructure and good practices.

Of course, verifying one or two accounts shouldn't be too time consuming, but once profiles contain 10 or 20 or more accounts, it just becomes a hassle. And that's where Keyoxide steps in, helping by automating the whole identity verification process, taking only a few seconds to verify large numbers of accounts.

Here, take a look at this example Keyoxide profile: [keyoxide.org/44a875dcbef777e79d7cca3ecc5764283d461ad1](https://keyoxide.org/44a875dcbef777e79d7cca3ecc5764283d461ad1). It contains a claim of a domain name: *keyoxide.org*.

To verify this manually, you could just use any DNS text resolver such as [this one](https://mxtoolbox.com/SuperTool.aspx?action=txt%3akeyoxide.org&run=toolpage). And sure enough, there is the fingerprint you are looking for: *44a875dcbef777e79d7cca3ecc5764283d461ad1*.

Keyoxide is simply trying to make your life easier by doing this for you, and repeating the process for all the other claims.

## How can you get started?

Currently, it's not too simple to create your own Keyoxide profile, you would have to know your way around your computer's command line. This is because Keyoxide is helping people verify other's identities, but does not help yet with setting up the identities in the first place.

This is being worked on and as soon as the apps are ready, we'll share the news right here on the blog!