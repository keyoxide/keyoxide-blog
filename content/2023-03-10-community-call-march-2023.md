+++
title = "Keyoxide Community Call: March 2023"
date = "2023-03-10 10:55:34"
+++

## Community call on March 18th, 2023

The Keyoxide project will be holding a Community Call on March 18th, 2023 at:

- 15:00 [UTC](https://time.is/UTC)
- 09:00 [US Central Time](https://time.is/CT)

Everybody is welcome to join the [jitsi meeting](https://meet.jit.si/moderated/7fe17e2a9457fa99fee1624708ba9f8662fc06e214de9dba9ed4f53a3c29f918) and ask questions.

The agenda and minutes will be available in the [keyoxide/community-calls repository](https://codeberg.org/keyoxide/community-calls/src/branch/main/calls/2023-03-18.md).