+++
title = "Keyoxide documentation update"
date = "2021-11-07 20:43:32"
+++

Today is a big day for documentation.

## A separate space for docs

The Keyoxide documentation is now on its own subdomain: [docs.keyoxide.org](https://docs.keyoxide.org).

It now also has a separate source repository: [codeberg.org/keyoxide/keyoxide-docs](https://codeberg.org/keyoxide/keyoxide-docs).

Both of these updates will make it easier to keep the docs up to date and even expand on it, adding more general-purpose guides and knowledge.

## The Ariadne Spec

The highlight of today's update is the launch of the Ariadne Spec, an announcement so big it got its own [blog post](/ariadne-spec/)!

It also has an entry in the [new docs](https://docs.keyoxide.org/getting-started/what-is-ariadne-id/).

The short version: the Ariadne Spec is an experimental community-driven living document that makes it easier to collectively improve the process that powers Keyoxide as well as start new independent implementations, libraries, apps and websites

An added benefit is that from now on, the OpenPGP notations will look like this:

```
proof@ariadne.id=https://someservice.tld/user
```

Have a look at the [blog post](/ariadne-spec/) and [docs entry](https://docs.keyoxide.org/getting-started/what-is-ariadne-id/) for more detailed information.

## Signing off

Thanks for tuning in today, I'm really happy with the current progress of the project. It took a few months but we now have separate docs ready for improvement and a democratic process to collectively write a document that could benefit many. Let's hope it accomplishes its goal!

Two announcements down, two more to go :)

Talk to you again in a few days,  
Yarmo