+++
title = "Keyoxide Project Update #1"
date = "2020-11-09 14:00:00"
+++

Time for the first big Keyoxide project update! A lot to cover, so let's get to
it.

## The Big Identity

Decentralized identity is coming. [DIDs](https://www.w3.org/TR/did-core/) are
coming. Awesome libraries like [IDX](https://idx.xyz/) are being published. Even
[Microsoft](https://www.microsoft.com/en-us/security/business/identity/own-your-identity)
seems on board.

My point is this: decentralized identity is an exciting field to be working on
right now and I'm committed to keep learning about this domain, its technologies
and contribute to our digital society's cure from the parasitic tech giants.
Keyoxide and the response it generated showed me this is within the realm of the
possible.

## A wild foundation appears!

To help me achieve this, I've decided to set up a foundation. Please welcome the
[Key to Identity Foundation](https://keytoidentity.foundation)! The foundation
will serve as an umbrella for a couple of identity-related projects to come,
which I will be glad to share more about as they progress. In fact, one of these
new projects is included in this update :)

Having a non-profit foundation also allows me to try and fully sustain the
project on donations and grants. I truly believe this model will help the
project and give it the best chance at making a significant change out there.

And as it turns out, I am not the only one who wants to see that happen&hellip;

## NLnet grant for Keyoxide development

The awesome people over at [NLnet](https://nlnet.nl/) have taken a good look at
the current status of the Keyoxide project, my plans for its future and it is
now my pleasure to announce they have decided to award me an
[NGI Zero grant](https://nlnet.nl/NGI0/) and fund the development!

I couldn't be more excited about this news. Keyoxide generated a lot of positive
feedback and ideas on how to improve it when it launched. Getting the
possibility to work on it full-time and build on the aspects that were important
to the community is a dream come true.

More information available on the
[NLnet website](https://nlnet.nl/project/Keyoxide/).

## Keyoxide endgame

I would like to expand on a point that is dear to me. Today's internet is in its
precarious state because we put faith in monopolistic forces that blossomed
under a lack of competition. The endgame of this endeavor is not just to create
a successful project. It is to build an ecosystem that will thrive on
competition and ultimately deliver the best experience for netizens.

It is what this in mind that I am releasing a new project today that should help
new projects get started in the decentralized identity world.

## doip.js

**DOIP** stands for Decentralized OpenPGP Identity Proofs, the technology that
enables the identity verification that Keyoxide performs.

**doip.js** is a Node.js library that enables any project to perform the same
tricks. It is even able to run directly in the browser!

What excites me most is that any contribution, like supporting new service
providers, is immediately available to all those projects and websites, not just
Keyoxide.

Documentation is available at [doip.rocks](https://js.doip.rocks/#/).

Code is licensed under Apache 2.0 and hosted by
[Codeberg.org](https://codeberg.org/keyoxide/doipjs).

## Building a community

Keyoxide now has a Matrix room, come hang out and discuss related topics!

Invite link: [#keyoxide:matrix.org](https://matrix.to/#/#keyoxide:matrix.org)

## The road ahead

We are just getting started here. A lot still needs to happen to make Keyoxide
and OpenPGP-based decentralized identity practical and useful for a larger
audience. With the NLnet grant and my newly-acquired ability to turn this
project into a full-time job, I foresee a bright future.

Hope to see you back for the next update!
