+++
title = "This Month in Keyoxide: October 2022"
date = "2022-10-30 16:27:50"
+++

## 🏠 Keyoxide project

### Keyoxide Mobile version 1.2.0

The Keyoxide mobile app's latest 1.2.0 update comes with a list of improvements
and bug fixes:

- Setting an user profile for quick access
- Improved layout (buttons, settings)
- Improved sharing of links
- Improved theming

The maintainer Berker also did a lot of code rewriting and restructuring to
prepare the app for some major planned improvements, that will make it much
easier to interact with Keyoxide profiles and we both honestly can't wait for
these updates to see the light of day. More announcements to follow!

And thanks again for all your time and dedication, Berker!

Relevant links:

- [Berker's announcement](https://berker.substack.com/p/keyoxide-mobile-120)
- [Keyoxide Mobile on F-Droid](https://f-droid.org/en/packages/org.keyoxide.keyoxide/)
- [keyoxide-flutter source code](https://codeberg.org/keyoxide/keyoxide-flutter)

### Widgets in the documentation

Keyoxide is complex. To use and to explain. It just has a complex nature. And
the documentation should help with that.

Slowly but surely, the documentation is becoming more interactive. A few weeks
ago, I introduced a
[simple widget](https://docs.keyoxide.org/understanding-keyoxide/identity-proof-formats/#Hashed_URI)
to help people with hashing their identity proofs.

Today, I am releasing the
[Keyoxide interactive guide](https://docs.keyoxide.org/guide/)
. The user experience is a work in progress and I am open to suggestions but it
should already give an impression on how interactive documentation can help
tackle tasks that are quite difficult to explain using static linear
documentation.

You can tell the guide what your Keyoxide profile is and what account you wish
to verify, and it will generate a list of steps that you need to perform to
actually do so. For example, if I wish to claim the `keyoxide.org` domain name
with my Keyoxide profile using a hashed proof, here are the required steps in a
[guide tailored to me](https://docs.keyoxide.org/guide/?profile_id=9f0048ac0b23301e1f77e994909f6bd6f80f485d&claim_uri=dns%3Akeyoxide.org%3Ftype%3DTXT&proof_type=argon2)
: just press the "Get guide" button!

The guide will also provide feedback whether both the claim and the proof were
correctly made. If so, you have successfully claimed a part of your online
identity!

This is a first step upon which more and more helpful interactive widgets can be
built.

Relevant links:

- [Keyoxide documentation on hashed proofs](https://docs.keyoxide.org/understanding-keyoxide/identity-proof-formats/#Hashed_URI)
- [Keyoxide interactive guide](https://docs.keyoxide.org/guide/)

## 🚧 DOIP libraries

The DOIP libraries are the brains behind the Keyoxide website and apps. They are
[open source](https://codeberg.org/keyoxide/doipjs) and available for everyone.

### Universal ActivityPub claim verification

Finally, we can verify all ActivityPub claims!

Previously, it had been a bit hit and miss. Pleroma and Mastodon would work
fine, Pixelfed would be recognized as Pleroma (due to their similar URL syntax),
others wouldn't work… And if an instance required signed HTTP requests, it
wouldn't work at all.

Now, this has all been solved: Keyoxide no longer makes a distinction between
the various fediverse server apps. If it speaks ActivityPub, Keyoxide can verify
it. All current and future servers. And all the HTTP requests Keyoxide makes are
now signed, so any instance with "secure mode" turned on can now have their
identities verified!

This update comes with a downside: all ActivityPub-related claims are now marked
as `activitypub`, the interface can no longer distinguish Pleroma, Pixelfed,
Peertube, Mastodon… This is being worked on!

Relevant links:

- [Mastodon documentation on HTTP signatures](https://docs.joinmastodon.org/spec/security/)

## 📄 Ariadne Specification

### Status of the Specification

First, a word on the status of the Ariadne Specification. It is currently
outdated as I have realized the approach I was using wasn't working — slow and
tedious to update. Next month, I will reboot the Ariadne specification with a
simpler approach, I will provide an update and request feedback as soon as I
have something to show.

### Ariadne Implementation Test Suite

This update, I am particularly excited about. Did you know the awesome folks
over at
[Sequoia PGP](https://sequoia-pgp.org/)
built an
[OpenPGP interoperability test suite](https://tests.sequoia-pgp.org/)
that they use to test and compare various OpenPGP implementations?

Well, I really like their concept. This month, I built my own version of this
but for software implementations of the Ariadne Specification. The space is
still young so there's only one implementation right now, the
[doip.js](https://codeberg.org/keyoxide/doipjs)
library. But hey, it's being tested right now and soon as others pop up, they
will become part of this automated testing as well!

I introduce the [Ariadne Implementation Test Suite](https://tests.ariadne.id/).

The way it works is simple: the test uses a list of identity claims known to
verify and tests the library against each one. The test suite asks to verify
correct and incorrect fingerprints: an implementation can't just reply
"verified" to every claim verification, it actually needs to do the work!

For this test suite to work, we need a standardized interface for all
implementations. Inspired by how the OpenPGP interoperability test suite
achieves this, I wrote a Stateless Command Line Interface for my library, the
[scli-doipjs](https://codeberg.org/keyoxide/scli-doipjs)
. This interface can be called from a terminal to perform the most fundamental
tasks that the library must be able to do. Currently, this is only verifying
identity claims but this will be expanded to reading cryptographic keys and
verifying entire profiles.

The syntax for the SCLI is still being considered, feel free to
[join the community](https://docs.keyoxide.org/community/)
and participate in the discussion.

Running the test suite yourself is pretty simple, you just need a bunch of
accounts with correct proofs (and access tokens for certain services like
Twitter, Telegram…):

- get the [source code](https://codeberg.org/keyoxide/ariadne-implementation-test-suite)
- make a [configuration file](https://codeberg.org/keyoxide/ariadne-implementation-test-suite/src/branch/main/config.template.toml)
- run ```cargo run -- --export-html```

Written in Rust 🦀

Relevant links:

- [OpenPGP interoperability test suite](https://tests.sequoia-pgp.org/)
- [Ariadne Implementation Test Suite](https://tests.ariadne.id/)
- [scli-doipjs](https://codeberg.org/keyoxide/scli-doipjs)

### Automated alerts for claims that suddenly stop working

The Ariadne Implementation Test Suite described above gave me another idea.

It has happened more that once during the last two years that I would wake up to
a series of complaints from people whose identity claims suddenly would no
longer verify. And sometimes, this was not due to a mistake on my part but
rather something happening with a service provider that changed something in
their API response, modified an API endpoint…

I have given the Ariadne Implementation Test Suite a second purpose: it will run
at a regular interval and send me notifications (using webhooks) when claims
that would previously verify no longer do so.

Getting this to work on your machine/server is pretty simple. Assuming you have
an endpoint for webhooks:

- get the [source code](https://codeberg.org/keyoxide/ariadne-implementation-test-suite)
- make a [configuration file](https://codeberg.org/keyoxide/ariadne-implementation-test-suite/src/branch/main/config.template.toml)
- run ```cargo run -- --trigger-webhooks``` at a regular interval (i.e. using cron jobs)

I have it set up on my server and this will help me detect errors when they
occur and hopefully even deploy fixes before the first bug reports arrive!

## Signing off

This has been a busy month, and in a way, I'm glad for this! So far, since the
project has started two years ago, I have been able to work on it full-time
thanks to a minimalist lifestyle and donations from the community and
[NLnet](https://nlnet.nl/)
.

And there is still lots to do!

If you like the Keyoxide project and would like to see it go further, I hope you
will consider making a donation as well, through
[Liberapay](https://liberapay.com/Keyoxide/)
(recurring) or
[Ko-fi](https://ko-fi.com/keyoxide)
(one-time). This helps me stay working full-time on Keyoxide and pay for the
servers. Thanks for considering!

Well, time to get back at it. The coming month will be dedicated to the Ariadne
Specification as well as releasing a few other improvements to Keyoxide I have
been working on. Hope to tell you more about this as soon as I can!

Yarmo