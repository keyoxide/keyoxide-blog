+++
title = "This Month in Keyoxide: November 2022"
date = "2022-11-30 17:17:39"
+++

## 🏠 Keyoxide project

### keyoxide-blue

The last few days, I've been developing [keyoxide.blue](https://keyoxide.blue), a new experimental Keyoxide web client, both as a distraction and as a way to test out a few potential new features. Yes, I will be using it as a playground, so:

1. Expect things to work
2. Be prepared for things to be broken
3. Do join the fun and suggest new interesting features

Of course, it's [open source](https://codeberg.org/keyoxide/keyoxide-blue) and all the identity verification logic runs in the browser (except for the calls to a proxy server) so you will need JS enabled for this one.

Putting some time into keyoxide-blue has had a few benefits. I could re-evaluate just how much effort was needed at the bare minimum to make a basic frontend for the [doip.js](https://codeberg.org/keyoxide/doipjs) library. Any additional work the frontend needed to perform that wasn't strictly about nicely presenting a profile is something perhaps the library should have done instead.

While this side project has shown me how to improve the doip.js library, it has also confirmed that little work is actually needed to integrate the library in any website and get identity verification going.

*So, Keyoxide Blue. What hidden message is lurking behind that name? What does it refer to? Surely it's a parody of Tw…*

I was thinking about the word *oxide*. I figured I could write it out as `#0c51de` (ocside) to turn that word into a hexadecimal color code. That color turns out to be blue. The specific blue used on the page.

## 🚧 DOIP libraries

### Fediverse post proofs

Not much happened this month in DOIP development. It is now possible to verify Fediverse accounts by posting the proof in a message and using the link to the post as the identity claim. Handy for people on Pixelfed who get limited space for their bio section!

## 📄 Ariadne Specification

### The New Ariadne Specification

This month, I have focused a significant share of my efforts to finishing and publishing a new Ariadne Identity Specification (AIS), a rewrite that I have been preparing for months. It's now public on [ariadne.id](https://ariadne.id).

The previous attempt at getting the Ariadne spec going was too convoluted and unwieldy. This time, it's a simple static website built around Markdown files. It's source code is on [Codeberg](https://codeberg.org/ariadne/ariadne-identity-specification). The contribution process still needs to be ironed out but at least, we can get going.

The new AIS implementation is based around three sections:

- the Core Specification: this describes the entire identity verification process
- the Related Specifications: these describe protocols that enhance the identity verification process
- the ARCs: these describe the different service providers and how to verify accounts on them

I think this arrangement makes a lot of sense. To write a new AIS-compatible project, one should only need to look at the Core Specification for the fundamental logic and the ARCs for how to apply it to different service providers.

For example, the Core Specification describes how identity claims and profiles are stored inside OpenPGP keys as notations or as cryptographic signatures. The ARCs then describe what to do with these identity claims. Once stable, the Core Specification should rarely be changed while a new ARC should be published every time a new online service is to be supported.

As for Related Specifications, these may describe protocols that aren't *fundamental identity verification logic* but still are of benefit to the process, like the Web Signature Directory specification.

### Web Signature Directory

I love the [Web Key Directory](https://datatracker.ietf.org/doc/draft-koch-openpgp-webkey-service/) specification. It's a simple way to publish OpenPGP keys on any server — abolishing the need for centralized keyservers — and discover them using email addresses as identifiers. It can be used on personal servers or as the basis for a [WKD-as-a-service](https://keys.openpgp.org/about/usage#wkd-as-a-service) platform.

This is the perfect infrastructure on which to build Keyoxide. Profiles are stored inside OpenPGP keys, OpenPGP keys are published and discovered using WKD. The user entirely controls the key and where it is stored. To delete your Keyoxide profile, just delete it from the WKD server.

But OpenPGP is finicky to work with. A great tool for the PC power user, a nightmare for anyone else. Building user-friendly tooling is also tricky as very few libraries support the exotic OpenPGP features Keyoxide needs.

So, in addition to Keyoxide-profiles-as-OpenPGP-keys, I want to add support for Keyoxide-profiles-as-cryptographic-signatures and have received a [NLnet grant](https://blog.keyoxide.org/nlnet-2/) to do just that.

While experimenting with signature profiles started nearly [two years ago](https://blog.keyoxide.org/keyoxide-project-update-2/), the idea has now matured and the time has come to definitively implement it and more importantly, build the infrastructure needed to host cryptographic signatures just as easily as cryptographic keys.

And what better inspiration for such an infrastructure than the Web Key Directory protocol?

I introduce [version 0 of the Web Signature Directory specification](https://ariadne.id/related/web-signature-directory-0/). It's largely based on WKD and adapted for the purposes of cryptographic signatures. It's tool-agnostic so it can work with any cryptographic tool such as OpenPGP, minisign, SSH, etc. It has an HTTP-based update protocol that is even designed to work with JS-less HTML forms. It's easily implemented on single-tenant servers and should be simple to turn into a multi-tenant WSD-as-a-service platform.

Since it's version 0, I do not recommend building implementations yet unless it's a toy implementation. This version is meant to receive critical review to fix issues and risks before we start building the real implementations.

If looking into specifications and protocols is your thing, I would much appreciate constructive criticism to improve WSD. As always, discussions are welcome on any of the [community channels](https://docs.keyoxide.org/community/).

## Signing off

This has been yet another big month for the project, even though you won't see the effects right away. Recent space billionaire drama has driven huge traffic to the Fediverse and a portion of those eyeballs have seen mentioning of the Keyoxide project.

With the increased exposure of people to Keyoxide has come the fair criticism on the complicated process of creating a profile. With the new protocols and specifications coming into place, we can now start building the tools and interfaces to make Keyoxide a much greater joy to get started with.

And I for one can't wait to see that happen.

Until next time,  
Yarmo

---

If you like the Keyoxide project and would like to see it go further, I hope you
will consider making a donation as well, through
[Liberapay](https://liberapay.com/Keyoxide/)
(recurring) or
[Ko-fi](https://ko-fi.com/keyoxide)
(one-time). This helps me stay working full-time on Keyoxide and pay for the
servers. Thanks for considering!