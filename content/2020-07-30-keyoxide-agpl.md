+++
title = "Keyoxide 1.0.0: switched to AGPL-v3"
date = "2020-07-30 12:48:24"
+++

## The big 1.0.0

Well, yes but no. It's actually a small update but with a MAJOR (get it? Because [semver](https://semver.org/)) change: the project has switched to the [AGPL-3.0-or-later](https://www.gnu.org/licenses/agpl-3.0.en.html) license.

When I started the [Keyoxide](https://keyoxide.org) project, it didn't have the scope and ambitions it has now. What begun as a tool to bring simple PGP operations directly to the user's browser&mdash;a side project like many others&mdash;has turned into a full-blown solution to prove online identity in a decentralized manner.

The project has also seen quite a warm welcome among the tech-savvy and privacy-minded as a partial replacement for alternatives like Keybase. More importantly, the project has started receiving contributions from other people. From that point on, as was pointed out to me by [@t0k@social.tchncs.de](https://social.tchncs.de/@t0k), a permissive license like I was using before will no longer do.

A copyleft license like [AGPL-3.0-or-later](https://www.gnu.org/licenses/agpl-3.0.en.html) is much better suited to protect the project and its contributors from getting the source code&mdash;including everyone's contributions&mdash;turned into a closed-source clone. Keyoxide is for the online citizenry and will remain so.

## Why 1.0.0?

Usually, the "big 1.0" is associated with a project coming out of a beta period or more generally, becoming a product that users can use without excessive bugs. This is not the case here.

The versioning of this project adheres to [semver](https://semver.org/): MAJOR-MINOR-PATCH. A license change such as this one might put certain people or organizations off from using it (it shouldn't&hellip; but it might) and could therefore be considered a breaking change which, according to semver, triggers a MAJOR release.

Hence 1.0.0.
