+++
title = "How to create an online identity, part 1: using OpenPGP"
date = "2023-03-08 19:58:24"
+++

## Introduction

You have seen someone's Keyoxide profile like [this one](https://keyoxide.org/id) and now you want to create your own page just like that, including verifiable identities!

Eventually, there will be at least three methods to create such a profile, including using only an app on a phone! For now, there is only one method: using OpenPGP in the command line. Not very user-friendly, agreed, but if you have some patience and the will to experiment (or you are already tech-savvy), we can get there!

If you'd rather not play around with the command line, please stay tuned and keep an eye on the blog.

This guide is a more expanded version of the [creating a profile](https://docs.keyoxide.org/getting-started/creating-profile/) on the Keyoxide documentation but contains the same steps and procedures.

## Get your system ready

Regardless of whether you are using the Linux, Windows or macOS operating system, your computer will have a "terminal" or a "command line". You can start it as you start any other application, the only difference is there are no buttons, no nice graphics, just a big textbox waiting for your input. It may seem scary but it really isn't!

Next, we need one tool: GnuPG, also known as gpg. This is an application like any other, except it doesn't have a graphical interface, and thus can only be used with the terminal or command line.

On Linux, `gpg` is usually already installed, if not use your distribution's package manager.

On Windows, install Gpg4win from the [offical website](https://www.gpg4win.org/). It comes with Kleopatra but sadly, that graphical tool doesn't do what we need for Keyoxide. Luckily, it also install `gpg` for the terminal/command line.

On macOS, install GPGTools from the [official website](https://gpgtools.org/).

Once the tools are installed, open a new terminal or command line.

## Generate a cryptographic key

*If you already have an OpenPGP key, feel free to skip this chapter.*

We use so-called cryptographic keys as the medium to create and distribute our online passports, as explained in the [nontechnical introduction to Keyoxide](/nontechnical-introduction/). To be more precise, we will generate a cryptographic keypair that follows the OpenPGP standard. Because it is standardized, it will be easy for others to interact with your cryptographic key.

A keypair consists of a private key which you will keep secret and stored only on your computer and a public key that you will publish openly. The private key is used to issue yourself your online passport, the public key is used to distribute your online passport.

In the terminal that you have opened, run the following command:

```bash
gpg --full-gen-key --expert
```

Not to worry, the `--expert` flag will not make it more complicated, it just allows us to select a specific type of key, namely the `Curve 25519`.

Next, type `10` then press enter to select `(10) ECC (sign only)`.

Next, type `1` then press enter to select `(1) Curve 25519`.

Great, now gpg knows what kind of key we want! Now, we need to set an expiration date. It is advisable to set an expiration date so that if you ever lose your private key, your online passport will invalidate itself after a certain period of time.

Of course, as long as you don't lose your private key, you will be able to keep moving the expiration farther in the future so it won't invalidate itself while you're still using it.

To set the expiration date two years later, type `2y` and press enter. Confirm with `y` and press enter. Set a reminder in your calendar for one year later to update the expiration date. The procedure is explained below.

Next, you enter your name and press enter. Note that this may be your real name or your online pseudonym. Remember that for your online passport, you don't actually need to be known by your real name! This is cyber space, be who you want to be!

You also need to enter a valid email address. This one needs to be valid and actually exist, as you will receive an email to confirm your key.

*Remember those future methods I told you will soon exist to create a profile? They won't need you to provide an email address! So if you'd rather not make your email address publicly known, please wait until the future methods are here.*

You will also be asked for a comment. This one is optional so feel free to skip it if you don't feel like including an arbitrary message.

To confirm all your "personal" data is correct, enter `O` (as in, **O**kay) and press enter.

You will now be asked for a password. Obviously, it is **strongly** recommended to use a password, and a unique one at that, stored in a password manager. You will need the password every time you make a change to your online passport.

After this, you should be all done! You now have the medium to hold and distribute your online passport.

Your cryptographic key has a unique identifier, also called the **fingerprint**. To find it, in the terminal, run:

```bash
gpg -K
```

You should find one (or more) keys, find the one you just created which has the name and email address you entered a few minutes ago. Next to the line starting with `sec`, you should see a sequence of 40 characters that looks like `5DC026DDD293736A3B305F42B6558943003E6A70` but with different characters. That is your key's fingerprint, copy this becuase you will need it a couple of times in the following procedures.

## Add identities to your cryptographic key

You have issued yourself your own online passport, congratulations! However, your passport is currently empty. You need to fill it with your online identities, your online accounts that you want to prove are yours.

To add an identity to your passport, you must perform two steps:

1. link your online passport to your online account (i.e. the *claim*);
2. link your online account to your online passport (i.e. the *proof*).

This creates a *bidirectional link* and is the concept that Keyoxide uses to prevent impersonation. Everyone can *claim* your online account, but only you can *prove* which claim is the true one!

### Proving a Fediverse account

It may seem counterintuitive, but it's easier to first *prove* an identity, then *claim* it.

So, let's say we want to prove & claim an account on the Fediverse, for example on a Mastodon server. Log into your Mastodon account, go to **Edit profile** and add the following URI:

```
openpgp4fpr:FINGERPRINT
```

to either:

1. one of your "metadata" links,
2. your "about me" section.

Of course, you replaced `FINGERPRINT` with your own fingerprint, right? :) That first part `openpgp4fpr:` tells Keyoxide where to go looking for fingerprints.

That's it, you have linked your Mastodon account to your cryptographic key. In other words, you have "proved" one of your online identities. Now, let's claim it!

### Claiming a Fediverse account

In the terminal (the same one, or a new one!), run:

```bash
gpg --edit-key FINGERPRINT
```

Again, replace `FINGERPRINT` with your own fingerprint.

Now, you write `notation` and press enter. gpg wants you to enter a notation, which is what we will use to put identity claims inside our online passports. One notation for each identity claim.

Write the following:

```
proof@ariadne.id=https://FEDIVERSE_SERVER/@USERNAME
```

Never change `proof@ariadne.id=`, this is a requirement by the people who invented and improved OpenPGP. You will have to replace `FEDIVERSE_SERVER` and `USERNAME`.

For example, the official Keyoxide project profile has the following notation:

```
proof@ariadne.id=https://fosstodon.org/@keyoxide
```

Press enter, and you should be asked for your password. Write `save`, press enter and you are done!

Your cryptographic key now links to your Mastodon acccount. You have "claimed" your online identity!

### Uploading your online passport

Congratulations, you now have a fully functional online passport, including an identity claim and an identity proof waiting for someone to verify your identity claim!

But no one can access it yet. Not even Keyoxide. Your online passport only lives on your computer. So let's upload it to a public keyserver!

Public keyservers are services people can use to upload their public keys, and this is how we will publish our online passport. Remember that we are only uploading the public half of our cryptographic keypair, this half is *meant* to be shared publicly, every one can read it but it can't be used to alter your online passport.

Keyoxide strongly recommends to use [keys.openpgp.org](https://keys.openpgp.org) for uploading your public keys. It's fully open source and developed by some very smart people. There are other keyservers out there but they have some flaws that don't play nicely with Keyoxide.

If this is your first time uploading your key to keys.openpgp.org, you need to perform the following steps:

1. In your terminal, run: `gpg --armor --output public.asc --export FINGERPRINT`;
2. Using your OS' file manager, locate the `public.asc` file you have created in step 1;
3. Go to [keys.openpgp.org/upload](https://keys.openpgp.org/upload) and upload the `public.asc` file;
4. Check the email address you entered when creating your key, it will receive a verification email, click on the link it contains.

Done! You can now visit [keyoxide.org](https://keyoxide.org) and search for your fingerprint or email address, both will work!

It may take up to an hour before your identity claim reliably shows the green tick.

There you go, you have created a Keyoxide profile! You can share your Keyoxide link with anyone and let them verify your claims for themselves.

### Uploading your online passport after the first time

Once you've done the whole email verification process, no need to ever go through these steps again (unless you add an email address). In the terminal, run:

```bash
gpg --keyserver hkps://keys.openpgp.org --send-keys FINGERPRINT
```

Done!

### Setting your avatar

If you want to add an avatar to your profile, create an account on [libravatar.org](https://www.libravatar.org/) and associate a picture with the email address you used for your cryptographic key.

### Closing remarks

Using Keyoxide really gets interesting once you claim & prove more than one online account, as that is how you establish a unified online presence. One Keyoxide profile to link all your online identities!

Have a Twitter account? [Claim and prove it](https://docs.keyoxide.org/service-providers/twitter/). Have a personal website? [Claim and prove it](https://docs.keyoxide.org/service-providers/dns/). Have a Reddit account? [Claim and prove it](https://docs.keyoxide.org/service-providers/reddit/).

Find many more services to verify in the [Keyoxide documentation](https://docs.keyoxide.org/).

Do you have another online account you wish to verify but Keyoxide doesn't support it yet? [Let us know](https://docs.keyoxide.org/community)!