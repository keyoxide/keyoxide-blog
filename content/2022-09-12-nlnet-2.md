+++
title = "New NLnet grant!"
date = "2022-09-12 13:38:52"
+++

## Keyoxide v2!

It's been a while since the last update but work has not stopped. And now, I am
so very excited to announce that NLnet has awarded Keyoxide a [second
grant](https://nlnet.nl/project/Keyoxide-signatures/index.html) to further
advance the various endeavours in which the Keyoxide project is involved.

The global focus of this new period of intensified development will be making it
easier for people to create and maintain their Keyoxide profiles. Much effort
will go into improving the support for a new type of profile — the so-called
**signature profile** — which one will be able to create using almost any of the
existing GnuPG tools out there. No more requiring people to interact with the
command line when they don't feel comfortable doing so.

Another point of focus will be **chat platforms**. The impact of impersonation
can be particularly severe on platforms where people interact with each other
directly. Keyoxide should thus be able to verify online identities on the major
communication platforms — provided the platform gives developers sufficient
access to do so.

Finally, this year will be all about documentation and specs. The Keyoxide
**documentation** can be much improved, both in terms of content and
accessibility. And the **Ariadne Spec** will get a do-over now that I have
realized the first iteration was attempting to accomplish way, way too much. A
more concise, to-the-point version will serve client implementations much better
— my own, and hopefully new ones developed by the community!

This coming period of development should make Keyoxide more stable and more
user-friendly for people in need of online inter-personal identification and
whom may not have the technical skills currently required to interact with
Keyoxide. This I have wanted for Keyoxide since the beginning and I
wholeheartedly thank the [NLnet foundation](https://nlnet.nl) and the [NGI
initiative](https://www.ngi.eu) for enabling me to accomplish this.

## New documentation

To get started off on the right foot, let's actually look at some progress made
during the last couple of weeks.

First, let's also take a moment to be overly honest here: I didn't like the old
documentation. I didn't like the content of the guides nor the way the articles
were structured. Both issues have been dealt with in the latest [Keyoxide
docs](https://docs.keyoxide.org). I have rewritten parts or entire guides and
moved sections around to paint a clearer picture.

As an added bonus — and this really shouldn't be a bonus, this should be standard: the
documentation can now be translated in different languages! If you feel like
contributing by helping translate some of the guides, feel free to [reach out to
the community](https://docs.keyoxide.org/community), we can help you get set up.

A lot still needs to happen on the documentation site but it's getting
somewhere!

## HTTP Proofs (and aliases)

Another potentially huge improvement is the new HTTP proof! Instead of adding a
cryptographic fingerprint to your biography — sure to make most people scratch
their head! — add the URL to your Keyoxide profile instead!

```
Hi! I'm Yarmo! This is my account on some website.
My online identity is managed on https://keyoxide.org/yarmo@yarmo.eu
```

This snippet would actually result in a verified identity claim on Keyoxide 🤯

For the curious: it works using an HTTP response header! More info in the [proof
formats documentation](https://docs.keyoxide.org/proof-formats).

But why stop there? If I forward a path on my personal domain to my Keyoxide
profile, I can just use that as a valid proof.

```
Verifying my identity: https://yarmo.eu/id
```

That's both much shorter and more helpful than a cryptographic fingerprint!

Of course, everything mentioned above works on
[keyoxide.org](https://keyoxide.org) but also any selfhosted instance.
Decentralization for the win 🚀

## Some things never change

Most important of all, the [community](https://docs.keyoxide.org/community) is
still here 🤗 on the forum, the IRC channel, the Matrix room… Besides the
increasingly interesting discussions, there have also been contributions coming
in like the recently added support for account verification on **Telegram** and
**Stack Exchange sites** — thank you again,
[Goldstein](https://codeberg.org/keyoxide/doipjs/pulls/24) and
[cherryblossom](https://codeberg.org/keyoxide/doipjs/pulls/23)!

Here's to lots more improvements!

Until later,  
Yarmo